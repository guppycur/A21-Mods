Key,English
gupMumBookBestiary,"A GuppyMumpfy Beastiary Sketch Book."
gupMumBookBestiaryDesc,"A bestiary collection of zombies you must kill.  Each unique zombie you kill gives you knowledge and adds to the book.  The book may not be shared, as it is YOUR experience that is added..  This book was collected from the body of last person assigned to take notes, your job is to finish it.  Killing a specific entity adds an entry and gives you a bonus on dismemberment."
